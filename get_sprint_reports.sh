#!/bin/bash

# get_sprint_reports.sh

# nvm use lts/erbium

# examples
# SPRINT=83  CSV_FILE_SBA=Sprint84_SBA.csv  CSV_FILE_BUGS=Sprint84_BUGS.csv  . get_sprint_reports.sh
# SPRINT=87  . get_sprint_reports.sh


DEFAULT_PATH="./reports"
DEFAULT_PREV_DATA_PATH="$DEFAULT_PATH/Sprint_BUGS_prev.csv"


CONFIGS_PATH=./configs




get_sprint_reports() {

    if [ -z $SPRINT ]; then
        echo "Не задана переменная выполнения 'SPRINT'" >&2
        return 1
    fi


    if [ -z $CSV_FILE_SBA ]; then
        echo "Не задана переменная выполнения 'CSV_FILE_SBA'" >&2

        CSV_FILE_SBA="Sprint"$SPRINT"_SBA.csv"

        echo "Ставлю значение по умолчанию: $CSV_FILE_SBA"


        read -p "Продолжить? Y/n " ACCEPT


        if [[ $ACCEPT != "Y" ]] && [[ $ACCEPT != "y" ]]; then

            echo "exit" >&2

            return 1
        fi
    fi


    if [ -z $CSV_FILE_BUGS ]; then
        echo "Не задана переменная выполнения 'CSV_FILE_BUGS'" >&2

        CSV_FILE_BUGS="Sprint"$SPRINT"_BUGS.csv"

        echo "Ставлю значение по умолчанию: $CSV_FILE_BUGS"


        read -p "Продолжить? Y/n " ACCEPT


        if [[ $ACCEPT != "Y" ]] && [[ $ACCEPT != "y" ]]; then

            echo "exit" >&2

            return 1
        fi
    fi

    if [ -z $PREV_DATA_PATH ]; then
        PREV_DATA_PATH=$DEFAULT_PREV_DATA_PATH
    fi


    echo
    echo "getting Sprint$SPRINT reports"

    echo "got CSV_FILE_SBA = '$CSV_FILE_SBA'"

    echo "got CSV_FILE_BUGS = '$CSV_FILE_BUGS'"

    echo "got PREV_DATA_PATH = '$PREV_DATA_PATH'"
    echo


    PATH_SBA=$DEFAULT_PATH/$CSV_FILE_SBA

    PATH_BUGS=$DEFAULT_PATH/$CSV_FILE_BUGS


    DIR=$HOME/work/files/team_lead/reports/sprint/$SPRINT



    if [ ! -d "$DIR" ]; then
        mkdir $DIR

        echo "creating directory $DIR"
    fi



    echo "generate Story Points report (Done)"
    echo "generate Story Points report (Done)" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config.yml  FILE_PATH=$PATH_SBA  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate Story Points report (In Test)"
    echo "generate Story Points report (In Test)" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_sba_in_test.yml  FILE_PATH=$PATH_SBA  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate Time Spent (BUGS) report"
    echo "generate Time Spent (BUGS) report" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_bugs.yml  PREV_DATA_PATH=$PREV_DATA_PATH  FILE_PATH=$PATH_BUGS  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate Estimation report"
    echo "generate Estimation report" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_estimation.yml  FILE_PATH=$PATH_SBA  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate High closed ratio report"
    echo "generate High closed ratio report" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_high.yml  FILE_PATH=$PATH_SBA  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate Due date closed ratio report"
    echo "generate Due date closed ratio report" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_due_date.yml  FILE_PATH=$PATH_SBA  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate BUGS were closed report"
    echo "generate BUGS were closed report" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_bugs_closed.yml  PREV_DATA_PATH=$PREV_DATA_PATH  FILE_PATH=$PATH_BUGS  ts-node main_parser.ts >> $DIR/report.csv


    echo >> $DIR/report.csv
    echo "generate BUGS highest report"
    echo "generate BUGS highest report" >> $DIR/report.csv
    echo >> $DIR/report.csv

    CONFIG=$CONFIGS_PATH/config_bugs_highest.yml  PREV_DATA_PATH=$PREV_DATA_PATH  FILE_PATH=$PATH_BUGS  ts-node main_parser.ts >> $DIR/report.csv



    echo succeed


    return 0
}


get_sprint_reports
