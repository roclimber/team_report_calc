

# Team report calculator


```sh
# use following command to switch node
nvm use lts/erbium
```

```sh
# use following command to install npm packages
npm i
```

```sh
# use following command to make calculations
SPRINT=84  CSV_FILE_SBA=Sprint84_SBA.csv  CSV_FILE_BUGS=Sprint84_BUGS.csv  . get_sprint_reports.sh
```

```sh
# other examples
SPRINT=83  CSV_FILE_SBA=JIRA_SBA_sprint83.csv  CSV_FILE_BUGS=JIRA_sprint83.csv  . get_sprint_reports.sh

SPRINT=85  CSV_FILE_SBA=Sprint85_SBA.csv  CSV_FILE_BUGS=Sprint85_BUGS.csv  . get_sprint_reports.sh

SPRINT=86  CSV_FILE_SBA=Sprint86_SBA.csv  CSV_FILE_BUGS=Sprint86_BUGS.csv  . get_sprint_reports.sh

SPRINT=100  . get_sprint_reports.sh
```


### Don't forget to update file "Sprint_BUGS_prev.csv" before running calculations

Usually do it after calculations
