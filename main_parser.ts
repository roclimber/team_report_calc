

const csv = require('csv-parser')

const yaml = require('js-yaml')
const fs = require('fs')

const R = require('ramda')


require('dotenv').config()





let DEFAULT_CONFIG_NAME = './config.yml'
let MAIN_FILE_PATH = './reports/Sprint84_SBA.csv'


let PREV_DATA_FILE_PATH = ''



try {

    DEFAULT_CONFIG_NAME = process.env.CONFIG || DEFAULT_CONFIG_NAME
    MAIN_FILE_PATH = process.env.FILE_PATH || MAIN_FILE_PATH

    PREV_DATA_FILE_PATH = process.env.PREV_DATA_PATH || PREV_DATA_FILE_PATH

} catch (e) {
    console.debug(e)
}





const readYamlConfig = async (fileName: string = DEFAULT_CONFIG_NAME): Promise<Config[]> => {

    let doc: Array<Config> = []


    try {

        doc = await yaml.load(fs.readFileSync(fileName, 'utf8'))

    } catch (e) {

        console.debug(e)
    }

    return doc
}







const loadCsvFile = async (filePath: string): Promise<string[]> => {

    const FILE_PATH = filePath


    const result: Array<string> = []


    const deferred: Promise<string[]> = new Promise(
        (resolve) => fs.createReadStream(FILE_PATH)
            .pipe(csv())
            .on('data', (data: string) => {
                result.push(data)
            })
            .on('end', () => {

                resolve(result)
            })
    )

    return deferred
}




class Report {

    constructor(
        public data: string[],
        public denominator: string,
        public numerator: string,
        public result: number,
        public resultString: string
    ) {}

}



enum ESTIMATION_INDEX {
    ESTIMATE,
    FACT
}

enum HIGH_CLOSED_INDEX {
    RESOLUTION,
    PRIORITY,
    MAIN = 1
}

const PRIORITY = {
    HIGH: 'High',
    HIGHEST: 'Highest'
}

const CONVERTION_VALUE = 60 * 60



class AbstractReportBuilder {

    private report: Report

    constructor(
        private data: Array<any>,
        private dataPrev: Array<any>
    ) {

        this.report = new Report(data, '', '', 0, '')
    }

    filter(field: string, value: string, reversed = false) {

        this.data = this.data.filter(item => reversed ? (item[field] != value) : (item[field] == value))

        return this
    }

    update(fieldName: string, fieldValue: string) {

        const data = []
        const oldData = R.clone(this.data)

        for (const item of oldData) {

            const prevValue = this.dataPrev
                .filter(itemPrev => itemPrev[fieldName] == item[fieldName])
                .reduce((prev, cur) => prev + Number(cur[fieldValue]), 0)

            const result = Number(item[fieldValue]) - prevValue

            item[fieldValue] = (result < 0) ? 0 : result

            data.push(item)
        }

        this.data = data

        return this
    }

    calcSumm(field: string) {

        this.report.result = this.data.reduce(
            (prev, cur, idx) => {

                const curValue = Number(cur[field])

                return prev + curValue
            },
            0
        )

        this.report.resultString = 'sum'

        return this
    }

    convertTime() {

        this.report.result /= CONVERTION_VALUE

        return this
    }

    calcEstimateFactRatio(fields: string[]) {

        if (fields.length > 1) {

            let estimateValue = 0
            let factValue = 0


            this.data.forEach(
                (item) => {

                    const estimateField = fields[ESTIMATION_INDEX.ESTIMATE]
                    const factField = fields[ESTIMATION_INDEX.FACT]

                    estimateValue += item[estimateField] / CONVERTION_VALUE
                    factValue += item[factField] / CONVERTION_VALUE
                },
                0
            )

            this.report.result = estimateValue / factValue

            this.report.numerator = String(estimateValue).replace('.', ',')
            this.report.denominator = String(factValue).replace('.', ',')

            this.report.resultString = 'estimateValue / factValue'
        }

        return this
    }

    calcClosedRatio(fields: string[], checkCondition: (value: string) => boolean) {

        if (fields.length > 1) {

            let closedValue = 0
            let mainValue = 0


            this.data.forEach(
                item => {

                    const resolutionField = fields[HIGH_CLOSED_INDEX.RESOLUTION]
                    const mainField = fields[HIGH_CLOSED_INDEX.MAIN]

                    const closedAmount = item[resolutionField] ? 1 : 0
                    const mainAmount = checkCondition(item[mainField]) ? 1 : 0

                    closedValue += (closedAmount && mainAmount) ? 1 : 0
                    mainValue += mainAmount
                },
                0
            )


            this.report.result = closedValue / mainValue

            this.report.numerator = String(closedValue)
            this.report.denominator = String(mainValue)

            this.report.resultString = 'closedValue / mainValue'
        }

        return this
    }


    calcHighClosedRatio(fields: string[]) {

        return this.calcClosedRatio(
            fields,
            (value: string) => (value == PRIORITY.HIGH)
        )
    }

    calcDueDateRatio(fields: string[]) {

        return this.calcClosedRatio(
            fields,
            (value: string) => !!value
        )
    }

    print(columns: string[]) {

        this.data.forEach(item => {

            const string = columns.reduce((prev, cur, idx) => `${prev}${prev ? '\t' : ''}${item[cur]}`, '')

            console.debug(string)
        })

        console.debug('\nitems amount:', this.data.length, '\n')

        this.report.numerator = String(this.data.length)
        this.report.resultString = 'items amount'

        return this
    }

    build() {

        this.report.data = this.data

        return this.report
    }
}


type Operation = {
    type: string,
    column?: string,
    columns?: string[]
}


type Config = {
    Common?: boolean,
    UseFile?: string,
    Filters?: any,
    Operation?: Operation,
    Print?: string[],
    UpdateData?: string[]
}



const OPERATION_TYPE = {
    SUMM: 'summ',
    BUGS_TIME: 'bugs_time',
    ESTIMATION: 'estimation',
    HIGH_CLOSED: 'high',
    DUE_DATE: 'due_date',
    BUGS_CLOSED: 'bugs_closed'
}

const LOP = {
    NOT: 'LOP_NOT'
}


class ReportBuilder extends AbstractReportBuilder {

    private config: Config


    constructor(
        csvFileData: Array<any>,
        config: Config,
        csvFileDataPrev: Array<any>
    ) {


        super(csvFileData, csvFileDataPrev)

        this.config = config
    }

    parseConfig() {

        const filters = this.config.Filters
        const { type, column, columns } = this.config.Operation

        for (const field in filters) {

            let value = filters[field]

            if (typeof value == 'string') {
                value = [value]
            }

            for (const item of value) {

                let filterValue = item
                const reversed = item.includes(LOP.NOT)

                if (reversed) {
                    filterValue = item.replace(LOP.NOT, '').trim()
                }

                this.filter(field, filterValue, reversed)
            }
        }

        let context = null
        let [fieldName, fieldValue]: Array<string> = ['', '']

        if (this.config.UpdateData) {

            [fieldName, fieldValue] = this.config.UpdateData
        }


        switch (type) {
            case OPERATION_TYPE.SUMM:
                context = this.calcSumm(column)
                break
            case OPERATION_TYPE.BUGS_TIME:
                context = this.update(fieldName, fieldValue).calcSumm(column).convertTime()
                break
            case OPERATION_TYPE.ESTIMATION:
                context = this.calcEstimateFactRatio(columns)
                break
            case OPERATION_TYPE.HIGH_CLOSED:
                context = this.calcHighClosedRatio(columns)
                break
            case OPERATION_TYPE.DUE_DATE:
                context = this.calcDueDateRatio(columns)
                break
            case OPERATION_TYPE.BUGS_CLOSED:
                context = this.update(fieldName, fieldValue).calcSumm(column).convertTime().filter(column, String(0), true)
        }

        const printColumns = this.config.Print

        if (printColumns && context) {
            context.print(printColumns)
        }

        return this
    }
}


interface ReportResult extends Report {
    name: string,
    resultValueString: string
}


const executeOperation = (operation: Config, name: string, csvFileData: Array<any>, csvFileDataPrev: Array<any>): ReportResult => {


    const report = new ReportBuilder(csvFileData, operation, csvFileDataPrev)
        .parseConfig()
        .build()


    const result = String(report.result).replace('.', ',')


    return {
        name,
        ...report,
        resultValueString: result
    }
}


interface PrintLineParameters {
    array: Array<ReportResult>,
    field: string
}


const printLine = ({ array, field }: PrintLineParameters): void => {

    const result = array.map((item) => item[field])

    console.debug(result.join('; '))
}


const FIELDS_TO_PRINT = [
    'name',
    'resultValueString',
    'resultString',
    'numerator',
    'denominator'
]


const printResults = (reportsArray: Array<ReportResult>) => {


    FIELDS_TO_PRINT.forEach(field => printLine({
        array: reportsArray,
        field
    }))
}



const COMMON_TASK = 'Common'


const runOperations = (config: Array<Config>, csvFileData: Array<any>, csvFileDataPrev: Array<any>) => {

    const reportsArray: Array<ReportResult> = []


    for (const field in config) {

        const operation = config[field]


        if (operation.Operation && !field.includes(COMMON_TASK)) {

            const result: ReportResult = executeOperation(operation, field, csvFileData, csvFileDataPrev)

            reportsArray.push(result)
        }
    }


    printResults(reportsArray)
}


const main = async () => {

    const config: Array<Config> = await readYamlConfig()

    const csvFileData: Array<any> = await loadCsvFile(MAIN_FILE_PATH)



    let csvFileDataPrev: Array<any> = []

    if (PREV_DATA_FILE_PATH) {

        csvFileDataPrev = await loadCsvFile(PREV_DATA_FILE_PATH)
    }


    try {

        runOperations(config, csvFileData, csvFileDataPrev)

    } catch (e) {
        console.debug(e)
    }

}


main()
